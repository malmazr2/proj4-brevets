# Project 4: Brevet time calculator with Ajax

Majed Almazrouei - malmazr2@uoregon.edu


## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help. Part of finishing this project is clarifying anything that is not clear about the requirements, and documenting it clearly.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

------------------------------------

The Program has been update to follow the ACP Rules.

- Applied the fixed closing times for brevert distinations and 0
- Applied the opening times for the checkpoints
- Applied the speeds for the closing and opening times according to the ACP website.
- The opening times uses maximum speed as given in the website for each range.
- The closing times uses the minumum speed as given in the website for each range.
- checkpoints above the brevert distination by 20% has been handled and it gives the output of the final distnation of the brevert
- Error handling the checkpoints more than the 20% by outputting the beginning time to tell the user there is an error.

---------------------------------------